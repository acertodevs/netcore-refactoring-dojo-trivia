﻿using System;

namespace MeuAcerto.Selecao.KataTrivia
{
    class Program
    {
        private static bool naoGanhou;

        public static void Main(String[] args)
        {
            Jogo jogo = new Jogo();

            jogo.AdicionaJogador("Huguinho");
            jogo.AdicionaJogador("Zezinho");
            jogo.AdicionaJogador("Luisinho");

            Random rand = new Random();

            do
            {
                jogo.JogaDado(rand.Next(6) + 1);

                if (rand.Next(9) == 7)
                {
                    naoGanhou = jogo.ErrouResposta();
                }
                else
                {
                    naoGanhou = jogo.AcertouResposta();
                }
            } while (naoGanhou);

            Console.ReadKey();

        }
    }
}
