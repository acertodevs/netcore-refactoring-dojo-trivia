﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MeuAcerto.Selecao.KataTrivia
{
    public class Jogo
    {
        List<string> jogadores = new List<string>();

        int[] posicoes = new int[6];
        int[] pontuacoes = new int[6];

        bool[] deCastigo = new bool[6];

        List<string> perguntasEntretenimento = new List<string>();
        List<string> perguntasCiencia = new List<string>();
        List<string> perguntasEsporte = new List<string>();
        List<string> perguntasHistoria = new List<string>();

        int jogadorAtual = 0;
        bool saiuDoCastigo;

        public Jogo()
        {
            for (int i = 0; i < 50; i++)
            {
                perguntasEntretenimento.Add("Pergunta de Entretenimento " + i);
                perguntasCiencia.Add(("Pergunta de Ciências " + i));
                perguntasEsporte.Add(("Pergunta de Esportes " + i));
                perguntasHistoria.Add(criaPerguntaHistoria(i));
            }
        }

        public String criaPerguntaHistoria(int index)
        {
            return "Pergunta de História " + index;
        }

        public bool podeIniciarPartida()
        {
            return (QuantidadeJogadores() >= 2);
        }

        public bool AdicionaJogador(String nomeJogador)
        {
            jogadores.Add(nomeJogador);
            posicoes[QuantidadeJogadores()] = 0;
            pontuacoes[QuantidadeJogadores()] = 0;
            deCastigo[QuantidadeJogadores()] = false;

            Console.WriteLine(nomeJogador + " foi adicionado");
            Console.WriteLine("Seu número é " + jogadores.Count);
            return true;
        }

        public int QuantidadeJogadores()
        {
            return jogadores.Count;
        }

        public void JogaDado(int valorDado)
        {
            Console.WriteLine(jogadores[jogadorAtual] + " é o jogador atual");
            Console.WriteLine("Ele rolou um " + valorDado);

            if (deCastigo[jogadorAtual])
            {
                if (valorDado % 2 != 0)
                {
                    saiuDoCastigo = true;

                    Console.WriteLine(jogadores[jogadorAtual] + " saiu do castigo");
                    posicoes[jogadorAtual] = posicoes[jogadorAtual] + valorDado;
                    if (posicoes[jogadorAtual] > 11) posicoes[jogadorAtual] = posicoes[jogadorAtual] - 12;

                    Console.WriteLine("A nova posição de "
                            + jogadores[jogadorAtual]
                            + " é "
                            + posicoes[jogadorAtual]);
                    Console.WriteLine("A categoria é " + CategoriaAtual());
                    FazPergunta();
                }
                else
                {
                    Console.WriteLine(jogadores[jogadorAtual] + " não saiu do castigo");
                    saiuDoCastigo = false;
                }

            }
            else
            {
                posicoes[jogadorAtual] = posicoes[jogadorAtual] + valorDado;
                if (posicoes[jogadorAtual] > 11) posicoes[jogadorAtual] = posicoes[jogadorAtual] - 12;

                Console.WriteLine("A nova posição de "
                        + jogadores[jogadorAtual]
                        + " é "
                        + posicoes[jogadorAtual]);
                Console.WriteLine("A categoria é " + CategoriaAtual());
                FazPergunta();
            }

        }

        private void FazPergunta()
        {
            if (CategoriaAtual() == "Entretenimento")
            {
                Console.WriteLine(perguntasEntretenimento.First());
                perguntasEntretenimento.Remove(perguntasEntretenimento.First());
            }
            if (CategoriaAtual() == "Ciencias")
            {
                Console.WriteLine(perguntasCiencia.First());
                perguntasCiencia.Remove(perguntasCiencia.First());
            }
            if (CategoriaAtual() == "Esportes")
            {
                Console.WriteLine(perguntasEsporte.First());
                perguntasEsporte.Remove(perguntasEsporte.First());
            }
            if (CategoriaAtual() == "Historia")
            {
                Console.WriteLine(perguntasHistoria.First());
                perguntasHistoria.Remove(perguntasHistoria.First());
            }
        }


        private String CategoriaAtual()
        {
            if (posicoes[jogadorAtual] == 0) return "Entretenimento";
            if (posicoes[jogadorAtual] == 4) return "Entretenimento";
            if (posicoes[jogadorAtual] == 8) return "Entretenimento";
            if (posicoes[jogadorAtual] == 1) return "Ciencias";
            if (posicoes[jogadorAtual] == 5) return "Ciencias";
            if (posicoes[jogadorAtual] == 9) return "Ciencias";
            if (posicoes[jogadorAtual] == 2) return "Esportes";
            if (posicoes[jogadorAtual] == 6) return "Esportes";
            if (posicoes[jogadorAtual] == 10) return "Esportes";
            return "Historia";
        }

        public bool AcertouResposta()
        {
            if (deCastigo[jogadorAtual])
            {
                if (saiuDoCastigo)
                {
                    Console.WriteLine("Certa a resposta!!!!");
                    pontuacoes[jogadorAtual]++;
                    Console.WriteLine(jogadores[jogadorAtual]
                            + " agora possui "
                            + pontuacoes[jogadorAtual]
                            + " ponto(s).");

                    bool vencedor = JogadorVenceu();
                    jogadorAtual++;
                    if (jogadorAtual == jogadores.Count) jogadorAtual = 0;

                    return vencedor;
                }
                else
                {
                    jogadorAtual++;
                    if (jogadorAtual == jogadores.Count) jogadorAtual = 0;
                    return true;
                }



            }
            else
            {

                Console.WriteLine("Certa a resposta!!!!");
                pontuacoes[jogadorAtual]++;
                Console.WriteLine(jogadores[jogadorAtual]
                        + " agora possui "
                        + pontuacoes[jogadorAtual]
                        + " ponto(s).");

                bool vencedor = JogadorVenceu();
                jogadorAtual++;
                if (jogadorAtual == jogadores.Count) jogadorAtual = 0;

                return vencedor;
            }
        }

        public bool ErrouResposta()
        {
            Console.WriteLine("Errou a resposta");
            Console.WriteLine(jogadores[jogadorAtual] + " está de castigo!");
            deCastigo[jogadorAtual] = true;

            jogadorAtual++;
            if (jogadorAtual == jogadores.Count) jogadorAtual = 0;
            return true;
        }

        private bool JogadorVenceu()
        {
            return !(pontuacoes[jogadorAtual] == 6);
        }
    }
}
