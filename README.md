# Trivia
2 a 6 jogadores
## 1. Componentes:
- 200 Cartas (50 de cada categoria)
- 36 Fichas coloridas de pontuação
6 Peças de jogadores
- 1 Tabuleiro
- 1 Dado

![Tabuleiro](tabuleiro.png)

## 2. Objetivo
Ser o primeiro jogador a obter 6 pontos.
## 3. Regras
Na sua vez de jogar, lance o dado e avance com sua peça o número de casas no tabuleiro, no sentido horário.
Qualquer que seja a categoria da casa onde você parar, você deverá responder a primeira pergunta do monte corresponde à categoria da casa que você está.
### Se acertar
Caso você acerte a pergunta, você ganha um ponto, e passa a jogada para o próximo jogador.
### Se errar
Caso você erre a pergunta, você fica de castigo. Enquanto você estiver de castigo, você não pode responder peguntas nem andar no tabuleiro, mas joga o dado normalmente. Caso você tire um número ímpar, você sai do castigo. Nesse momento, você anda no tabuleiro e responde a pergunta normalmente.